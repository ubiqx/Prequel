_Hello, world!_

Prequel
=======

<img align="right" src="vanity/Prequel.icon.png">
Parts, pieces, and tools for implementing and working with the PeerDist
suite of protocols that underly
[BranchCache](https://technet.microsoft.com/en-us/library/hh831696.aspx).

Overview
--------

[BranchCache](https://technet.microsoft.com/en-us/library/hh831696.aspx)
is a distributed caching system that provides light-weight wide area
network acceleration.

<a href="#"><img align="left" height="96" src="vanity/Dog.png"></a>
The protocols and protocol extensions that underly BranchCache are known
as PeerDist; PeerDist protocol documentation is available from Microsoft
(see the references, below).

The purpose of the Prequel project (this project) is to provide tools
and libraries that will help others implement, mess with, and understand
PeerDist.

Glossary
--------

See [[MS-CCROD]](https://msdn.microsoft.com/en-us/library/dd303704.aspx)
for a more authoritative
[glossary](https://msdn.microsoft.com/en-us/library/hh441992.aspx) of
PeerDist terms.  The list of terms presented here includes a few of our
own.

<dl>
  <dt>Segment</dt>
  <dd>A chunk of content that has an associated identifier (Segment ID).
  Segment IDs and Segments are key:value pairs.
  </dd>
</dl><dl>
  <dt>Segment ID</dt>
  <dd>Also known as a <i>fingerprint</i> or <i>HoHoDk</i>.  HoHoDk probably
  means "Hash of HoD and Key", where the HoD is the Segment Hash of
  Data...or something like that.  In any case, it's the value that (in
  theory) uniquely identifies a Segment.
  </dd>
</dl><dl>
  <dt>Segment Hash of Data (HoD)</dt>
  <dd>The meaning of this term depends on the version of PeerDist.  In
  version 1.0, segments are broken down into blocks and the a hash is
  computed over each block.  The HoD is the hash of those hashes.  In
  version 2.0, the HoD is assumed to be the hash of the content segment
  itself.
  </dd>
</dl><dl>
  <dt>Segment Secret</dt>
  <dd>This is the signed Segment Hash of Data (HoD).  The Segment Secret is
  generated as the hash of the HoD and the Server Secret.
  </dd>
</dl><dl>
  <dt>Server Passphrase</dt>
  <dd>This is Prequel terminology.<br/><a
  href="https://msdn.microsoft.com/en-us/library/dd303704.aspx">[MS-PCCRC]</a>
  refers to this value as "an arbitrary length binary string stored on
  the server".  It is, basically, a secret password, the hash of which
  is known as the Server Secret and is used as a signing key.
  </dd>
</dl><dl>
  <dt>Server Secret</dt>
  <dd>Also known as the <a
  href="https://msdn.microsoft.com/en-us/library/jj665526.aspx">Server
  Secret Key</a>, this is simply the SHA-256 hash of the Server
  Passphrase.
  </dd>
</dl>

References
----------

<a href="#"><img align="right" height="260" src="vanity/DancingMan.png"></a>

[[BranchCache](https://technet.microsoft.com/en-us/library/hh831696.aspx)]:
BranchCache Overview

[[MS-CCROD]](https://msdn.microsoft.com/en-us/library/hh441913.aspx):
Content Caching and Retrieval Protocols Overview

[[MS-PCCRC]](https://msdn.microsoft.com/en-us/library/dd303704.aspx):
Peer Content Caching and Retrieval: Content Identification

[[MS-PCCRD]](https://msdn.microsoft.com/en-us/library/dd357279.aspx):
Peer Content Caching and Retrieval: Discovery Protocol

[[MS-PCCRR]](https://msdn.microsoft.com/en-us/library/dd340715.aspx):
Peer Content Caching and Retrieval: Retrieval Protocol

[[MS-PCCRTP]](https://msdn.microsoft.com/en-us/library/dd304322.aspx):
Peer Content Caching and Retrieval: Hypertext Transfer Protocol (HTTP)
Extensions

[[MS-PCHC]](https://msdn.microsoft.com/en-us/library/dd303737.aspx):
Peer Content Caching and Retrieval: Hosted Cache Protocol

More Code
---------

The folks at [iPXE.org](http://www.ipxe.org/) implemented parts of PeerDist.
It appears, however, that they removed that code from the project.  Here's
the only link I could find:  https://dox.ipxe.org/peerdist_8c.html

Status
------

This project has been stagnant for a while.

It didn't generate a lot of buzz, initially, so I lost interest.  There
is _a lot more code_ scattered about that needs to be collected,
reorganized, cleaned up, and committed.

The code, as it is being cleaned up, will be
[Doxygenated](https://ubiqx.gitlab.io/Prequel/).

<a href="#"><img
   width="100%" align="center" src="vanity/SleepingGuards.png"></a>

____
$Id: README.md; 2020-10-30 15:01:47 -0500; crh$
