/* ========================================================================== **
 *                                SHA256oSSL.c
 *
 * Copyright:
 *  Copyright (C) 2012 by Christopher R. Hertel
 *
 * Email: crh@ubiqx.org
 *
 * $Id: SHA256oSSL.c; 2017-09-14 12:37:50 -0500; Christopher R. Hertel$
 *
 * -------------------------------------------------------------------------- **
 *
 * Description:
 *  Generate an SHA256 sum from input, and output the result as raw binary.
 *
 * -------------------------------------------------------------------------- **
 *
 * License:
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * -------------------------------------------------------------------------- **
 *  This code was developed in participation
 *  with the Protocol Freedom Information Foundation.
 *  http://www.protocolfreedom.org/
 * -------------------------------------------------------------------------- **
 *
 * Notes:
 *
 *  This is very simple code, using OpenSSL, that will generate an SHA256
 *  hash from data input on <stdin>.  It was written to generate PeerDist
 *  Server Secret keys for the Prequel project.
 *
 *  cc -o SHA256oSSL SHA256oSSL.c -lcrypto
 *
 * ========================================================================== **
 */

#include <stdlib.h>       /* Standard library.  */
#include <stdio.h>        /* I/O stuff.         */
#include <stdarg.h>       /* Variable arg lists.*/
#include <errno.h>        /* For <errno>.       */
#include <string.h>       /* For strerror(3).   */
#include <openssl/sha.h>  /* OpenSSL SHA tools. */
#include <openssl/evp.h>  /* See EVP_DigestInit(3SSL).  */


/* -------------------------------------------------------------------------- **
 * Macros:
 *
 *  Err()   - This is a conceited little macro that simply replaces
 *            fprintf( stderr, ... ) with something shorter and easier
 *            to type.
 *  Say()   - This is a conceited little macro that simply replaces
 *            printf(3) with something shorter and easier to type.
 *  ErrStr  - Shorthand for the error message associated with <errno>.
 */

#define Err( ... ) (void)fprintf( stderr, __VA_ARGS__ )
#define Say( ... ) (void)printf( __VA_ARGS__ )
#define ErrStr (strerror( errno ))


/* -------------------------------------------------------------------------- **
 * Defined Constants:
 *  bSIZE - Read buffer size.
 */

#define bSIZE 4096


/* -------------------------------------------------------------------------- **
 * Static Functions:
 */

void Fail( char *fmt, ... )
  /* ------------------------------------------------------------------------ **
   * Format and print a failure message on <stderr>, then exit the process.
   *
   *  Input:  fmt - Format string, as used in printf(), etc.
   *          ... - Variable parameter list.
   *
   *  Output: none
   *
   *  Notes:  Exits the process returning EXIT_FAILURE.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  va_list ap;

  va_start( ap, fmt );
  (void)fprintf( stderr, "Failure: " );
  (void)vfprintf( stderr, fmt, ap );
  va_end( ap );
  exit( EXIT_FAILURE );
  } /* Fail */


/* -------------------------------------------------------------------------- **
 * Program Mainline.
 */

int main( int argc, char *argv[] )
  /* ------------------------------------------------------------------------ **
   * Program Mainline.
   *
   *  Input:  argc  - You know what this is.
   *          argv  - You know what to do.
   *
   *  Output: EXIT_SUCCESS
   *          or, if you are having a really bad day, EXIT_FAILURE.
   *
   * ------------------------------------------------------------------------ **
   */
  {
  static unsigned char bufr[bSIZE];
  size_t               result;
  EVP_MD_CTX          *ctx;

  /* Preliminaries. */
  if( argc > 1 )
    {
    Err( "Usage:\t%s < [input] > [output]\n", argv[0] );
    Err( "\tThis program reads from standard input and writes to standard\n" );
    Err( "\toutput.  The input is passed through the SHA256 algorithm.\n" );
    Err( "\tThe resulting hash is written to output in raw binary format.\n" );
    return( EXIT_FAILURE );
    }
  Err( "[Reading from <stdin>, output to <stdout>.]\n" );

  /* Allocate a context thingy. */
  if( NULL == (ctx = (EVP_MD_CTX *)calloc( 1, sizeof( EVP_MD_CTX ) )) )
    Fail( "Memory allocation failed.\n" );

  /* Initialize the digest context. */
  if( 0 == EVP_DigestInit( ctx, EVP_sha256() ) )
    Fail( "Digest context could not be initialized.\n" );

  /* Read and hash the input. */
  do
    {
    result = fread( bufr, 1, bSIZE, stdin );
    if( !EVP_DigestUpdate( ctx, bufr, result ) )
      Fail( "Digest update error.\n" );
    } while( bSIZE == result );

  /* Finalize and write.  */
  if( 0 == EVP_DigestFinal( ctx, bufr, NULL ) )
    Fail( "Unable to finalize the SHA256 digest.\n" );
  result = fwrite( bufr, 1, 32, stdout );

  /* All done. */
  return( EXIT_SUCCESS );
  } /* main */

/* ========================================================================== */
